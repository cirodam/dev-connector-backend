const express = require('express');
const {check, validationResult} = require('express-validator/check');
const User = require('../../models/User');
const gravatar = require('gravatar');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('config');

const router = express.Router();

const checks = [    
    check('name', 'Name is required').not().isEmpty(),
    check('email', 'Please include a valid email').isEmail(),
    check('password', 'Password requires at least six characters').isLength({min: 6})
];

// @route POST api/users
// @desc register a new user
// @access public
router.post('/', checks, async (req, res) => {

    //Check for errors
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        return res.status(400).json({errors: errors.array()});
    }

    const {name, email, password} = req.body;
    try{

        //Check if user already exists
        let user = await User.findOne({email});
        if(user){
            return res.status(400).json({errors: [{msg: 'User already exists'}]});
        }

        //Get gravatar given email
        const avatar = gravatar.url(email, {s: '200', r: 'pg', d: 'mm'});
        user = new User({name, email, avatar, password});

        //Encrypt password and save new user
        const salt = await bcrypt.genSalt(10);
        user.password = await bcrypt.hash(password, salt);
        await user.save();

        //return jwt
        const payload = {
            user: {
                id: user.id
            }
        };
        jwt.sign(payload, config.get('jwt_secret'), {expiresIn: 3600}, (err, token) => {
            if(err){
                throw err;
            }else{
                res.status(200).json({token});
            }
        });

    }catch(err){
        res.status(500).send('Server Error');
    }

});

module.exports = router;