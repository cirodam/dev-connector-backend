const express = require('express');
const auth = require('../../middleware/auth');
const {check, validationResult} = require('express-validator/check');
const jwt = require('jsonwebtoken');
const config = require('config');
const bcrypt = require('bcryptjs');
const User = require('../../models/User');

const router = express.Router();

// @route GET api/auth
// @desc Get a user
// @access private
router.get('/', auth, async (req, res) => {

    try{
        const user = await User.findById(req.user.id).select('-password');
        res.json(user);
    }catch(err){
        console.error(err.message);
        res.status(500).json('Server Error');
    }

});

const checks = [    
    check('email', 'Please include a valid email').isEmail(),
    check('password', 'Password is required').exists()
];

// @route POST api/auth
// @desc Login user and get token
// @access public
router.post('/', checks, async (req, res) => {

    //Check for errors
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        return res.status(400).json({errors: errors.array()});
    }

    const {email, password} = req.body;
    try{

        //Check if there exists a user with that password
        let user = await User.findOne({email});
        if(!user){
            return res.status(400).json({errors: [{msg: 'Invalid Credentials'}]});
        }

        //Check if passwords match
        const isMatch = await bcrypt.compare(password, user.password);
        if(!isMatch){
            return res.status(400).json({errors: [{msg: 'Invalid Credentials'}]});
        }

        //return jwt
        const payload = {
            user: {
                id: user.id
            }
        };
        jwt.sign(payload, config.get('jwt_secret'), {expiresIn: 3600}, (err, token) => {
            if(err){
                throw err;
            }else{
                res.json({token});
            }
        });

    }catch(err){
        res.status(500).send('Server Error');
    }

});

module.exports = router;